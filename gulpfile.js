var gulp = require('gulp');
var concatCss = require('gulp-concat-css');
var minifyCSS = require('gulp-minify-css');
var stripCssComments = require('gulp-strip-css-comments');
var notify = require('gulp-notify');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var replace = require('gulp-replace');
var size = require('gulp-filesize');
var addsrc = require('gulp-add-src');

var jsValidate = require('gulp-jsvalidate');
var clean = require('gulp-clean');
var rename = require('gulp-rename');
var jasmine = require('gulp-jasmine-phantom');
var jshint = require('gulp-jshint');
var jscs = require('gulp-jscs');


gulp.task('utest', function() {
    gulp.src('spec.js')
      .pipe(jasmine({
         integration: true,
         vendor: ["serviceVoice/serviceVoice.js"]
      }));
});

gulp.task('jshint', function() {
    gulp.src('serviceVoice/serviceVoice.js')
      .pipe(jshint())
      .pipe(jshint.reporter('default'));
});


gulp.task('minjs', function () {
    gulp.src(['serviceVoice/serviceVoice.js'])
     .pipe(uglify())
     .pipe(rename('serviceVoiceMIN.js'))
     .pipe(gulp.dest('serviceVoice/'))
     .pipe(notify('MinJS Done!'))
});


gulp.task('mincss', function () {
    gulp.src(['serviceVoice/serviceVoice.css'])
      .pipe(stripCssComments())
      .pipe(minifyCSS(''))
      .pipe(rename('serviceVoiceMIN.css'))
      .pipe(gulp.dest('serviceVoice/'))
      .pipe(notify('MinCss Done!'))
});



gulp.task('valid', function () {
    gulp.src('serviceVoice/serviceVoice.js')
      .pipe(jsValidate());
});


gulp.task('jscs', function () {
    gulp.src('serviceVoice/serviceVoice.js')
      .pipe(jscs());
});

gulp.task('default', ['minjs', 'mincss']);


gulp.task('watch',function(){
    gulp.watch('serviceVoice/serviceVoice.js',['minjs']);
    gulp.watch('serviceVoice/serviceVoice.css',['mincss']);
})

