
'use strict';
var ServiceVoice = (function(window,document){

		var obj = {
			info:{
				name 		: '',
				email 		: '',
				data 		: '',
				url 		: location.href,
				service 	: '',
				os			: navigator.appVersion,
				_token	: ''
			},
			config:{
				mode		: "dev",
				id_location	: '',
			},
			style:{
				background : function(color){
					obj.tmpl.getElementsByClassName('SV_feedback')[0].style.backgroundColor = ''+color+'';
					obj.tmpl.getElementsByClassName('SV_faux-textbox-textarea')[0].style.backgroundColor = ''+color+'';
					obj.tmpl.getElementsByClassName('SV_pane-header')[0].style.backgroundColor = ''+color+'';
					obj.tmpl.getElementsByClassName('SV_form_mail_inp')[0].style.cssText='background-color: '+color+'!important';
					obj.tmpl.getElementsByClassName('SV_uv-popover-tail')[0].style.borderTop = ''+color+'';
					obj.tmpl.getElementsByClassName('SV_email_error')[0].style.backgroundColor = ''+color+'';
				},
				buttonColor : function(color){
					var buts = obj.tmpl.getElementsByClassName('SV_but-widget-submit');
					for(var i=0;i<buts.length;i++){
						buts[i].style.backgroundColor = ''+color+'';
					}
				},
				buttonHover : function(color){
					var css='.SV_but-widget-submit:hover{background-color:'+color+'!important}'+
							'.SV_form_mail_inp:focus,textarea[id="SV_voice_text"]:focus{border-color:'+color+'!important}'+
							'.SV_form3_disp{color:'+color+'}';
					var style=document.createElement('style');
					if (style.styleSheet) {style.styleSheet.cssText=css;}
					else { style.appendChild(document.createTextNode(css));	}
					var tmp = obj.tmpl.getElementsByClassName('SV_cont')[0];
					tmp.insertBefore(style,tmp.firstChild );
				},
				buttonFontColor : function(color){
					var buts = obj.tmpl.getElementsByClassName('SV_but-widget-submit');
					for(var i=0;i<buts.length;i++){
						buts[i].style.color = ''+color+'';
					}
				},
				fontFamily : function(family){
					var buts = obj.tmpl.getElementsByClassName('SV_font');
					for(var i=0;i<buts.length;i++){
						buts[i].style.fontFamily = ''+family+'';
					}
				},
				fontColor : function(color){
					var buts = obj.tmpl.getElementsByClassName('SV_Color');
					for(var i=0;i<buts.length;i++){
						buts[i].style.color = ''+color+'';
					}
				},
				location_id : function(id){
					obj.config.id_location = id;
				},
				position : function(pos){
					var tail = obj.tmpl.getElementsByClassName('SV_uv-popover-tail')[0];
					var feed = obj.tmpl.getElementsByClassName('SV_feedback')[0];
					
					switch (pos) {
								case "right": 	tail.className += ' SV_tail_right';
											  	feed.className += ' SV_feed_right';
											break;
								case "left": 	tail.className += ' SV_tail_left';
												feed.className += ' SV_feed_left';
											break;
								case "top": 	tail.className += ' SV_tail_top';
												feed.className += ' SV_feed_top';
											break;
								case "bottom": 	tail.className += ' SV_tail_bottom';
												feed.className += ' SV_feed_bottom';
											break;
							}
				},
				link_id : function(id){
					var id_elem = document.getElementById(id);
					var svg = obj.tmpl.getElementsByTagName('svg')[0];
					id_elem.onclick = svg.onclick;
					id_elem.className += ' SV_link';
					id_elem.style.cssText += ';opacity:1!important;';
					svg.parentNode.removeChild(svg);
				},
			},
			set : function(a,b){
				b||0;
				
				if(typeof a =='object'){
					for(var k in a ){
						this.info[k] = a[k];
					}
				}
				else if(a == 'mode'){
					this.config[a] = b;
				}
				else{
					this.info[a] = b;
				}
			},
			custom : function(a,b){
				b||0;
				
				if(typeof a =='object'){
					for(var k in a ){
						this.style[k](a[k]);
					}
				}
				else{
					this.style[a](b);
				}
			},
			events:{
				displaySuccess : function(){
					var el = document.getElementsByClassName('SV_over-layer2')[0].style;
					var suc = document.getElementsByClassName('SV_form3')[0].style;
					var title = document.getElementsByClassName('SV_pane-title')[0].style;
					el.display = (el.display=="block")? 'none':'block';
					suc.display = (el.display=='none')? 'block':'none';
					title.display = (el.display=='none')? 'none':'block';
					return this;
				},
				displayMode : function(){
					if(location.search.indexOf("sv=0")>0) return false;
					else return true;
				},
				openClose : function(){
					var el = document.getElementsByClassName("SV_feedback")[0].style;
					var svg = document.getElementsByClassName("SV_link")[0].style;
					el.display=(el.display=='none')? 'block': 'none';
					svg.opacity=(el.display=='block')? '1':'0.8';
					return this;
				},
				getFocus : function(x){
					setTimeout(function() { document.getElementById(''+x+'').focus(); },0);
					return this;
				},
				slidePage : function(side){
					var cont = document.getElementsByClassName("SV_over-layer2")[0];
					cont.style.transitionDuratation = "0.5s";

					switch (side) {
						case "left":
						setTimeout(function(){ cont.style.left = "-325px"; },0); 
						setTimeout(function(){ obj.events.getFocus('SV_voice_text2'); },500);
						break;
						case "right": 
						setTimeout(function(){ cont.style.left = "0px"; },0);
						setTimeout(function(){ obj.events.getFocus('SV_voice_text'); },500);
						break;
					}
				},
				initEmail : function(){
					if(!obj.info.email ){
						var but1 = obj.tmpl.getElementsByClassName('SV_but-widget-submit')[0];
						but1.innerText = "Далее";
						but1.setAttribute("onclick","ServiceVoice.events.slidePage('left')");
					}
				},
				checkMode : function(){
					if(obj.config.mode=='dev') {
						obj.tmpl.getElementsByTagName('link')[0].href = "/js/serviceVoice/serviceVoice.css";
					}
				},
				checkEmail : function(){
					var mail = document.getElementById("SV_voice_text2");
					var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

					if(mail.value.match(re) === null) {
						document.getElementsByClassName("SV_email_error")[0].style.display = "block";
						return false;
					}
					else { 
						document.getElementsByClassName("SV_email_error")[0].style.display = "none"; 
						obj.events.slidePage('right');
						obj.info.email = document.getElementById("SV_voice_text2").value;
						obj.events.send();
					}
				},
				send : function(){
					var but1,but2,attr1,attr2;

					function toVoice(){
							setTimeout( function(){
								obj.info.name = document.getElementsByClassName('SV_form_name')[0].value;
								obj.info._token = document.getElementById('xtoken').content;
								
								var json = JSON.stringify( obj.info );
								var http = new XMLHttpRequest();
								var url;

								if(obj.config.mode){
									switch (obj.config.mode) {
										case "dev":
											url = "//laravel:81/api/servicevoice"; break;
										case "master": 
											url = "//servicedesk.dcp24.ru/api/ticket/servicevoice"; break;
									}
								}
								else { url = "//dev.servicedesk.dcp24.ru/api/ticket/servicevoice"; }

								url = location.protocol + url;
								url = '/api/servicevoice';
								
								// if ("withCredentials" in http) {
								// 	http.open("POST", url, true);
								// 	http.send(json);
								// } else if (typeof XDomainRequest != "undefined") {
								// 	http = new XDomainRequest();
								// 	http.open("POST", url);
								// 	http.send(json);
								// } else {
								// 	http = null;
								// }
								$.post( url,obj.info );

								obj.events.displaySuccess(); 
								document.getElementById("SV_voice_text2").value= "";
								document.getElementById("SV_voice_text").value = "";
								//document.getElementById("SV_include_screenshot").checked = false; 
								obj.info.data = "";
								//obj.info.screenshot = "";

								but1.setAttribute('onclick',attr1);
								but2.setAttribute('onclick',attr2);
							},0);
						}


					if( (document.getElementById("SV_voice_text").value ) && obj.info.email ){
						but1 = document.getElementById('SV_Vbut1');
						but2 = document.getElementById('SV_Vbut2');
						attr1 = but1.getAttribute('onclick');
						but1.setAttribute('onclick','');
						attr2 = but2.getAttribute('onclick');
						but2.setAttribute('onclick','');


						document.getElementsByClassName("SV_feedback")[0].style.display='';
						obj.info.data = document.getElementById("SV_voice_text").value; 

						// if(document.getElementById("SV_include_screenshot").checked){
						// 	// html2canvas(document.body, {
						// 	// 	onrendered: function(canvas) {
						// 	// 		obj.info.screenshot = canvas.toDataURL("image/png");
						// 	// 		obj.info.screenshot = obj.info.screenshot.substring(22);
						// 	// 		//toVoice(); 
						// 	// 	}
						// 	// });

						// } else {
						// 	//toVoice();
						// }
						toVoice();
					}
				},
				render : function(){ 
					if(obj.config.id_location) {
						var cont = obj.tmpl.getElementsByClassName('SV_cont')[0];
						cont.style.position = "inherit"
						var parent = document.getElementById(obj.config.id_location);
						parent.appendChild(cont);
					}
					else{ document.getElementsByTagName('body')[0].appendChild(obj.tmpl.firstChild); }
					
				}
			},
				tmpl : (function(){
		var str =''
		+'<div class="SV_cont" style="width: 39px;height: 39px;position:fixed;bottom: 10px;right: 20px;" > '
		+'	<svg id="SV_svg_circle" class="SV_link" width="37px" height="40px"  style="opacity: 0.8;" onclick="ServiceVoice.events.openClose().getFocus(\'SV_voice_text\')" viewBox="0 0 48 48" version="1.1" xmlns="http://www.w3.org/2000/svg">'
		+'		<path fill="#1abc9c" d=" M 22.53 0.00 L 26.09 0.00 C 37.52 0.99 47.27 10.76 48.00 22.25 L 48.00 26.14 C 47.03 37.55 37.26 47.25 25.81 48.00 L 21.84 48.00 C 10.35 47.00 0.61 37.07 0.00 25.52 L 0.00 21.91 C 1.01 10.39 10.93 0.60 22.53 0.00 Z" />'
		+'		<path fill="#ffffff" d=" M 11.56 13.91 C 11.38 12.45 12.39 10.90 13.98 11.06 C 20.69 10.95 27.41 10.94 34.11 11.07 C 35.67 10.91 36.61 12.50 36.43 13.89 C 36.47 18.57 36.64 23.28 36.33 27.95 C 34.72 31.21 30.22 28.53 27.61 30.19 C 24.10 32.33 20.81 34.81 17.35 37.02 C 18.17 34.56 19.02 32.10 19.86 29.64 C 17.27 29.17 13.15 30.90 11.69 27.96 C 11.35 23.29 11.53 18.59 11.56 13.91 Z" />'
		+'	</svg>'
		+'    <div class="SV_feedback" style="display:none">'
		+'    	<div class="SV_feed_cont">'
		+'			    <div class="SV_pane-header">'
		+'		        	<div class="SV_pane-title SV_font SV_Color">Написать в поддержку</div>'
		+'		        	<button class="SV_toolbar-button SV_viewport-top-right-button" onclick="ServiceVoice.events.openClose()">'
		+'							<i class="SV_icon SV_close-icon"></i>'
		+'		        	</button>'
		+'			    </div>'
		+'			    <div id="SV_over-layer">'
		+'				    	<div class="SV_over-layer2" style="display:block">'
		+'					      	<div class="SV_feed_form">'
		+'					        	<div class="SV_pane-body" >'
		+'										<div>ФИО <input type="text" class="SV_form_name"></div>'
		//+'										<div>E-mail <input type="email" class="SV_form_email"></div>'
		+'					                	<div class="SV_font SV_Color">Хотите задать вопрос или появилась идея? Напишите нам:</div>'
		+'					              	<div class="SV_textarea_mar"  tabindex="0" >'
		+'										<textarea class="SV_faux-textbox SV_faux-textbox-textarea SV_font SV_Color" id="SV_voice_text"></textarea>'
		+'					              	</div>'
		+'						            <div class="SV_elasticbar">'
		+'						                <span class="SV_elasticbar-item">'
		+'												<div class="SV_but-widget-submit SV_font" id="SV_Vbut1" onclick="ServiceVoice.events.send()">Отправить</div>'
		+'						                </span>'
		+'						            </div>'
		+'						        </div>'
		+'					        </div>'
		+'					        <div class="SV_2menubox">'
		+'					        	<div class="SV_feed_mailform">'
		+'					        		<div class="SV_form_mail SV_font SV_Color">Ваш электронный адрес</div>'
		+'					        		<input type="text" class="SV_form_mail_inp SV_font SV_Color" id="SV_voice_text2" placeholder="Адрес электронной почты">'
		+'					        		<div class="SV_email_error SV_font">Введите верынй email</div>'
		+'					        	</div>'
		+'					        		<div class="SV_form_but2">'
		+'						        		<div class="SV_elasticbar-item">'
		+'								          <button class="SV_flat-button SV_text-wrap SV_font" type="button" id="SV_but_back" onclick="ServiceVoice.events.slidePage(\'right\')">'
		+'								            <i class="SV_icon SV_back-icon SV_font"></i> Редактировать сообщение'
		+'								          </button>'
		+'								        </div>'
		+'							            <div class="SV_elasticbar SV_elasticbar-item SV_h-elastic">'
		+'							                <span class="SV_elasticbar-item">'
		+'													<div class="SV_but-widget-submit SV_but-widget-right SV_font" id="SV_Vbut2" onclick="ServiceVoice.events.checkEmail()">Отправить</div>'
		+'							                </span>'
		+'							            </div>'
		+'							        </div>'
		+'					        </div>'
		+'					    </div>'
		+'							<div class="SV_form3">'
		+'								<p class="SV_form3_disp SV_font">Ваша заявка успешно отправлена</p>'
		+'								<div class="SV_elasticbar SV_elasticbar3">	'
		+'									<span class="SV_elasticbar-item">	'
		+'	    								<div class="SV_but-widget-submit SV_font" id="SV_Vbut_ok" onclick="ServiceVoice.events.openClose().displaySuccess()">Ок, спасибо</div>	'		
		+'									</span>'
		+'								</div>'
		+'							</div>'
		+'			    </div>'
		+'		    	<div class="SV_uv-popover-tail"></div>'
		+'	    </div> '
		+'  	</div>'
		+'</div>'
		;

		var el = document.createElement('div');
		el.innerHTML = str;
		return el;

	})(),
	init : function(){
		if(!obj.events.displayMode()) return false;

		//obj.events.checkMode();
		obj.events.initEmail();

		obj.events.render();


	}

};


return obj;
})(window,document);
