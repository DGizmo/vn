<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentNews extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('comment_news', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('news_id')->references('id')->on('posts');
			$table->integer('user_id')->references('id')->on('users');
			$table->string('user_name')->references('name')->on('users');
			$table->text('text');
			$table->boolean('status');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('comment_news');
	}

}
