<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->string('email')->unique();
			$table->string('password', 60);

			$table->string('firstname', 60)->nullable();
			$table->string('surname', 60)->nullable();
			$table->string('lastname', 60)->nullable();
			$table->string('phone', 20)->nullable();
			$table->string('adress', 100)->nullable();
			$table->boolean('getNews');
			$table->boolean('getRefresh');

			$table->rememberToken();

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
