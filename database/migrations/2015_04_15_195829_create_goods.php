<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGoods extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('goods', function(Blueprint $table)
		{
			$table->increments('id');
			// $table->integer('parent_cat_id')->references('parent_category')->on('category');
			$table->integer('category_id')->references('id')->on('category');
			$table->string('name', 100);
			$table->string('good_url', 100)->unique();
			//$table->integer('articul');
			//$table->text('composition');
			//$table->text('description');
			$table->integer('price');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('goods');
	}

}
