<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarts extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('carts', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->references('id')->on('users');
			$table->integer('order_id')->references('id')->on('orders');
			
			//$table->integer('good_id')->references('id')->on('goods');;
			$table->string('good_name',100);
			$table->integer('qty');
			$table->integer('price');
			$table->string('size',100);
			$table->string('color',100);
			$table->string('img',200);
			
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('carts');
	}

}
