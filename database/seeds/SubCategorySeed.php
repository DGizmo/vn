<?php
use Illuminate\Database\Seeder;

class SubCategorySeed extends Seeder {

	public function run(){
		DB::table('sub_categories')->delete();

		$category = array(
			array(
				'subcat_id'=> 1,
				'parent_id' => 1,
				'subcat_name' => 'Футболки, лонгсливы и топы',
				'subcat_url' => 'gshirts'
			),
			array(
				'subcat_id'=> 2,
				'parent_id' => 1,
				'subcat_name' => 'Платья и сарафаны',
				'subcat_url' => 'gdress'
			),
			array(
				'subcat_id'=> 3,
				'parent_id' => 1,
				'subcat_name' => 'Юбки',
				'subcat_url' => 'gskirts'
			),
			array(
				'subcat_id'=> 4,
				'parent_id' => 1,
				'subcat_name' => 'Штанишки',
				'subcat_url' => 'gpants'
			),
			array(
				'subcat_id'=> 5,
				'parent_id' => 1,
				'subcat_name' => 'Толстовки',
				'subcat_url' => 'gsmocks'
			),
			array(
				'subcat_id'=> 6,
				'parent_id' => 2,
				'subcat_name' => 'Футболки',
				'subcat_url' => 'bshirts'
			),
			array(
				'subcat_id'=> 7,
				'parent_id' => 2,
				'subcat_name' => 'Штаны',
				'subcat_url' => 'bpants'
			),
			array(
				'subcat_id'=> 8,
				'parent_id' => 2,
				'subcat_name' => 'Толстовки',
				'subcat_url' => 'bsmocks'
			),
			array(
				'subcat_id'=> 9,
				'parent_id' => 3,
				'subcat_name' => 'Девочки',
				'subcat_url' => 'agirls'
			),
			array(
				'subcat_id'=> 10,
				'parent_id' => 3,
				'subcat_name' => 'Мальчики',
				'subcat_url' => 'aboys'
			)
		);

		DB::table('sub_categories')->insert($category);
	}

}
