<?php
use Illuminate\Database\Seeder;

class MainCategorySeed extends Seeder {

	public function run(){
		DB::table('main_categories')->delete();

		$category = array(
			array(
				'cat_id'=> 1,
				'cat_name' => 'Девочки',
				'cat_url' => 'girls'
			),
			array(
				'cat_id'=> 2,
				'cat_name' => 'Мальчики',
				'cat_url' => 'boys'
			),
			array(
				'cat_id'=> 3,
				'cat_name' => 'Аксессуары',
				'cat_url' => 'accessories'
			)
		);

		DB::table('main_categories')->insert($category);
	}

}
