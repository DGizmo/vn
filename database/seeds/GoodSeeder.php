<?php
use Illuminate\Database\Seeder;

class GoodSeeder extends Seeder {

	public function run(){
		DB::table('goods')->delete();

		$goods = array(
			array(
				'category_id' => 1,
				'name' => 'T-shirt',
				'good_url' => 'test_url',
				//'articul' => 1,
				//'composition' => 'god damn shirt',
				//'description' => 'omg what a beautiful shirt',
				'price' => 790,
				'created_at' => new DateTime,
				'updated_at' => new DateTime
			),//date('d-m-Y h:m:s'),
			array(
				'category_id' => 1,
				'name' => 'Skirt',
				'good_url' => 'test_skirt',
				//'articul' => 2,
				//'composition' => 'god damn skirt',
				//'description' => 'omg what skirt!?',
				'price' => 1090,
				'created_at' => new DateTime,
				'updated_at' => new DateTime
			),
			array(
				'category_id' => 6,
				'name' => 'Boy_mayk',
				'good_url' => 'bmyak',
				'price' => 1090,
				'created_at' => new DateTime,
				'updated_at' => new DateTime
			),
			array(
				'category_id' => 6,
				'name' => 'Boy_toy',
				'good_url' => 'btoy',
				'price' => 1090,
				'created_at' => new DateTime,
				'updated_at' => new DateTime
			)
		);

		DB::table('goods')->insert($goods);
	}

}
