<?php
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder {

	public function run(){
		DB::table('users_new')->delete();

		$users = array(
			array(
				'name' => 'test',
				'password' => Hash::make('123'),
				'email' => 'test@test.ru'

			)
		);

		DB::table('users_new')->insert($users);
	}

}
