<?php
use Illuminate\Database\Seeder;

class CategorySeed extends Seeder {

	public function run(){
		DB::table('categories')->delete();

		$category = array(
			array(
				'id'=> 1,
				'parent_category' => NULL,
				'type' => 'Девочки',
				'url' => 'girls'
			),
			array(
				'id'=> 2,
				'parent_category' => NULL,
				'type' => 'Мальчики',
				'url' => 'boys'
			),
			array(
				'id'=> 3,
				'parent_category' => NULL,
				'type' => 'Аксессуары',
				'url' => 'accessories'
			),
			array(
				'id'=> 4,
				'parent_category' => 1,
				'type' => 'Футболки, лонгсливы и топы',
				'url' => 'shirts'
			),
			array(
				'id'=> 5,
				'parent_category' => 1,
				'type' => 'Платья и сарафаны',
				'url' => 'dress'
			),
			array(
				'id'=> 6,
				'parent_category' => 1,
				'type' => 'Юбки',
				'url' => 'skirts'
			),
			array(
				'id'=> 7,
				'parent_category' => 1,
				'type' => 'Штанишки',
				'url' => 'pants'
			),
			array(
				'id'=> 8,
				'parent_category' => 1,
				'type' => 'Толстовки',
				'url' => 'smocks'
			),
			array(
				'id'=> 9,
				'parent_category' => 2,
				'type' => 'Футболки',
				'url' => 'shirts'
			),
			array(
				'id'=> 10,
				'parent_category' => 2,
				'type' => 'Штаны',
				'url' => 'pants'
			),
			array(
				'id'=> 11,
				'parent_category' => 2,
				'type' => 'Толстовки',
				'url' => 'smocks'
			),
			array(
				'id'=> 12,
				'parent_category' => 3,
				'type' => 'Девочки',
				'url' => 'girls'
			),
			array(
				'id'=> 13,
				'parent_category' => 3,
				'type' => 'Мальчики',
				'url' => 'boys'
			)
		);

		DB::table('categories')->insert($category);
	}

}
