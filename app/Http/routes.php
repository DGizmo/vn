<?php

//use View;
use App\Models\Item;



//Route::get('/', 'WelcomeController@index');

Route::get('home', 'HomeController@index');
Route::get('/', 'HomeController@index');


Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);


Route::get('/news', 'PostController@index');

Route::get('/news/{slug}','PostController@getPost');
Route::post('/news/{slug}','PostController@postComment');


Route::get('/todo',array( 'middleware' => 'auth', 'uses' => 'TodoController@index'));
Route::post('/todo','TodoController@postIndex'); //->before('csrf');
// Route::get('/todo/login','AuthController@getLogin');
// Route::post('/todo/login','AuthController@postLogin');

Route::get('/todo/new','TodoController@getNew');
Route::post('/todo/new','TodoController@postNew');

Route::bind('item', function($value, $route){
    return Item::where('id', $value)->first();
});
Route::get('/todo/delete/{item}','TodoController@getDelete');


Route::get('/goods/{good}','CategoryController@getGood');



Route::get('/catalog','CategoryController@getInfo');
Route::get('/catalog/{cat}','CategoryController@getInfo');
Route::get('/catalog/{cat}/{sub}','CategoryController@getInfo');
Route::get('/catalog/{cat}/{sub}/{good}','CategoryController@getInfo');
Route::post('/catalog/{cat}/{sub}/{good}','CategoryController@addGood');

Route::get('/cart',array( 'middleware' => 'auth', 'uses' => 'CartController@index'));
Route::get('/cart/delete/{id}','CartController@destroy'); // не православно

Route::get('/cart/order',array( 'middleware' => 'auth', 'uses' =>'OrderController@index'));
Route::post('/cart/order','OrderController@create');

Route::get('/cabinet',array( 'middleware' => 'auth', 'uses' => 'LKController@index'));
Route::post('/cabinet','LKController@edit');

Route::post('/search','HomeController@search');


Route::get('/auth/logout','AuthController@logout');





Route::get('/admin','Admin\AdminController@index');
Route::get('/admin/yml','Admin\AdminController@getYml');

Route::get('/admin/show','Admin\AdmCategoryController@show');
Route::get('/admin/show/{name}','Admin\AdmCategoryController@showAll');
Route::get('/admin/delete','Admin\AdmCategoryController@delete'); // delete
Route::get('/admin/add','Admin\AdmCategoryController@getAddForm'); 
Route::post('/admin/add','Admin\AdmCategoryController@add'); 

Route::get('/admin/news','Admin\NewsController@index');
Route::get('/admin/news/delete/{id}','Admin\NewsController@delete'); //delete
Route::get('/admin/news/edit/{id}','Admin\NewsController@edit');
Route::post('/admin/news/edit/{id}','Admin\NewsController@save');
Route::get('/admin/news/add','Admin\NewsController@getForm');
Route::post('/admin/news/add','Admin\NewsController@add');

Route::get('/admin/comments','Admin\CommentsController@index');
Route::get('/admin/comments/add','Admin\CommentsController@getForm');
Route::post('/admin/comments/add','Admin\CommentsController@add');
Route::get('/admin/comments/delete/{id}','Admin\CommentsController@delete'); //delete


Route::get('/admin/orders','Admin\OrderController@index'); //delete
Route::get('/admin/orders/show/{id}','Admin\OrderController@cart'); //delete



Route::get('/admin/servicevoice','Admin\AdminController@getServiceVoice');
Route::post('/api/servicevoice','Admin\AdminController@serviceVoice');



