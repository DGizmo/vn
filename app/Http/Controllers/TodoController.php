<?php namespace App\Http\Controllers;

use App\Models\Item;
use View,Auth,Input,Redirect,Validator;

class TodoController extends Controller {

	public function index(){
		$items = Auth::user()->items;

		return View::make('todo.index',array(
			'items'=>$items
		));
	}

	public function postIndex(){
		$id = Input::get('item_id');
	    $item = Item::findOrFail($id);
	 
	    $userId = Auth::user()->id;
	    if($item->user_id ==  $userId) {
	        $item->mark();
	    }
	 
	    return Redirect::to('todo');
	}


	public function getNew() {
		return View::make('todo.new');
	}

	public function postNew() {
	    $rules = array('name' => 'required|min:5|max:200');
	     
	    $validator = Validator::make(Input::all(), $rules);
	     
	    if($validator->fails()){
	        return Redirect::to('/todo/new')->withErrors($validator);
	    }
	     
	    $item = new Item();
	    $item->name = Input::get('name');
	    $item->user_id = Auth::user()->id;
	    $item->save();
	     
	    return Redirect::to('todo');
	}


	public function getDelete(Item $item) {    
	    if($item->user_id == Auth::user()->id){
	        $item->delete();
	    }
	     
	    return Redirect::to('todo');
	}



}
