<?php namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Good;

use View,Request,DB,Redirect,Cart,Input,Auth;

class CategoryController extends Controller {



	public function getInfo($cat=null,$sub=null,$good=null)
    {

        if($cat && $sub && $good){
        	$result = DB::table('main_categories') // НАДО СОЗДАТЬ МОДЕЛЬ
			->where('cat_url','=',$cat)
			->join('sub_categories', 'cat_id', '=','parent_id')
			->where('subcat_url','=',$sub)
			->join('goods', 'subcat_id', '=','category_id') 
			->where('good_url','=',$good)
			->get();
			// dd($result[0]->name);
			if(!$result) return  Redirect::back();
        	
        	return View::make('catalog.good')->with('good',$result[0]);

        }
        elseif($cat && $sub){
        	$result = DB::table('main_categories')
			->where('cat_url','=',$cat)
			->join('sub_categories', 'cat_id', '=','parent_id')
			->where('subcat_url','=',$sub)
			->join('goods', 'subcat_id', '=','category_id') 
			->get();
        }
        elseif($cat ){
        	if (!in_array($cat, ['girls','boys','accessories'])) {
    			return  Redirect::back();
    		}

        	$result = DB::table('main_categories')
			->where('cat_url','=',$cat)
			->join('sub_categories', 'cat_id', '=','parent_id')
			->join('goods', 'subcat_id', '=','category_id') 
			->get();
        }
        else{
        	$result = DB::table('main_categories')
			->join('sub_categories', 'cat_id', '=','parent_id')
			->join('goods', 'subcat_id', '=','category_id') 
			->get();
		}

		return View::make('catalog.main')->with('cat',$result);
		//надо переделать чтобы каждая свою вьюху возвращала
    }

    public function addGood()
    {
    	// Cart::destroy();
    	$res = Input::all();
    	//$this->tmp;
    	// dd($res);
    	// dd($res=>item_id);

    	Cart::add($res['item_id'], $res['name'], $res['qty'], $res['price'], 
    		array('size' => $res['size'], 'color' => $res['color'], 'img' => $res['img']));
		return  Redirect::back();
		//return View::make('catalog.good')->with('good',$result[0]);
    	//dd($result);
    }
}
