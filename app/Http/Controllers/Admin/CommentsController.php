<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Post;
use View,Request,Response,Redirect,Input,DB;

class CommentsController extends Controller {

	public function index(){
		// dd(1);
		$comments = DB::table('comment_news')->get();
		return View::make('admin.comments.list')->with(['comments'=>$comments]);
	}

	public function delete($id){
		// dd(1);
		$post = DB::table('comment_news')
			->where('id','=',$id)->delete();
		return Redirect::to('/admin/comments');
	}

	public function getForm(){
		
		$posts = Post::get();
		return View::make('admin.comments.add')->with(['posts'=>$posts]);
	}

	public function add(){
		$inp = Input::all();

		DB::table('comment_news')->insert(array(
			'news_id'=>$inp['new_id'],'user_id'=>0,'user_name'=>'Админ','text'=>$inp['text'],
			'created_at'=> new \Datetime, 'updated_at'=> new \Datetime,
				));

		return Redirect::to('/admin/comments');
	}
}
