<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Post;
use View,Request,Response,Redirect,Input;

class NewsController extends Controller {

	public function index(){
		// dd(1);
		$posts = Post::get();
		return view('admin.news.news')->with(['posts'=>$posts]);
	}

	public function delete($id){
		$post = Post::where('id','=',$id)->delete();
		return Redirect::back();
	}

	public function edit($id){
		$post = Post::where('id','=',$id)->firstOrFail();
		return view('admin.news.edit')->with(['post'=>$post]);
	}

	public function save($id){
		$res = Input::all();
		// dd($res);
		$post = Post::where('id','=',$id)
			->update(['title'=>$res['name'],'slug'=>$res['url'],
					'body'=>$res['text'], 'updated_at'=> new \Datetime]);
		return Redirect::to('/admin/news');
	}

	public function getForm(){
		return view('admin.news.add');
	}

	public function add(){
		$res = Input::all();
		$post = new Post();
		$post->title = $res['name'];
		$post->slug = $res['url'];
		$post->body = $res['text'];
		$post->created_at = new \Datetime;
		$post->updated_at = new \Datetime;
		$post->save();
		
		return Redirect::to('/admin/news');
	}
}