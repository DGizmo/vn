<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use View,Request,DB,Redirect,Input,Auth;

class AdmCategoryController extends Controller {

	public function show(){
		$req = Request::all();

		if( $req ){
			
			if (array_key_exists('cat', $req) && is_numeric($req['cat']) ) {
				$result = DB::table('main_categories')
					->where('cat_id','=',$req['cat'])
					->join('sub_categories', 'cat_id', '=','parent_id')
					->join('goods', 'subcat_id', '=','category_id') 
					->get();

				$result ? $header = 'Категория '.$result[0]->cat_name : '';

			}elseif(array_key_exists('subcat', $req) && is_numeric($req['subcat']) ){
				$result = DB::table('sub_categories')
					->where('subcat_id','=',$req['subcat'])
					->join('main_categories', 'parent_id', '=','cat_id')
					->join('goods', 'subcat_id', '=','category_id') 
					->get();

				$result ? $header = 'Подкатегория '.$result[0]->subcat_name : '';

			}elseif(array_key_exists('good', $req) && is_numeric($req['good']) ){
				$result = DB::table('goods')
					->where('id','=',$req['good'])
					->join('sub_categories', 'category_id', '=','subcat_id') 
					->join('main_categories', 'parent_id', '=','cat_id')
					->get();

				$result ? $header = $result[0]->name : '';
			}
		}else{
			return Redirect::to('/admin');
		}

		if(!$result) return Redirect::to('/admin');

		return View::make('admin.category.show')->with(['cat'=>$result,'header'=>$header]);
	}
	
	public function showAll($name){
		if($name == 'category'){
			$result = DB::table('main_categories')->get();
			$header = array('name' =>'Категории','url' =>'cat');
		}elseif($name == 'subcategory'){
			$result = DB::table('main_categories')
				->join('sub_categories', 'cat_id', '=','parent_id')
				->get();
			$header = array('name' =>'Подкатегории','url' =>'subcat');
		}elseif($name == 'goods'){
			$result = DB::table('main_categories')
				->join('sub_categories', 'cat_id', '=','parent_id')
				->join('goods', 'subcat_id', '=','category_id')
				->get();
			$header = array('name' =>'Товары','url' =>'good');

		}else{
			Redirect::back();
		}

		// dd(2);
		return View::make('admin.category.list')->with(['cat'=>$result,'header'=>$header]);
	}

	public function delete(){

		$req = Request::all();

		if( $req ){
			
			if (array_key_exists('cat', $req) && is_numeric($req['cat']) ) {
				DB::table('main_categories')
					->where('cat_id','=',$req['cat'])
					->delete();

			}elseif(array_key_exists('subcat', $req) && is_numeric($req['subcat']) ){
				$result = DB::table('sub_categories')
					->where('subcat_id','=',$req['subcat'])
					->delete();

			}elseif(array_key_exists('good', $req) && is_numeric($req['good']) ){
				$result = DB::table('goods')
					->where('id','=',$req['good'])
					->delete();
			}
			return Redirect::back();
		}
		
		return Redirect::to('/admin');
	}

	public function getAddForm(){

		$req = Request::all();
		// dd($req);
		if( $req ){
			
			if ($req['params'] == 'cat' ) {
				return View::make('admin.category.add')->with(['req'=>$req]);
			
			}elseif ($req['params'] == 'subcat' ) {
				$result = DB::table('main_categories')->get();
				return View::make('admin.category.add')->with(['req'=>$req,'parent'=>$result]);
			
			}elseif ($req['params'] == 'good' ) {
				$cat = DB::table('main_categories')->get();
				$subcat = DB::table('sub_categories')->get();
				return View::make('admin.category.add')->with(['req'=>$req,'cat'=>$cat,'sub_cat'=>$subcat]);
			}
			
		}
		
		return Redirect::to('/admin');
	}

	public function add(){
		
		$inp = Input::all();

		if( $inp['name'] && $inp['url']){

			if ($inp['table'] == 'category') {
				DB::table('main_categories')->insert(array(
					'cat_name'=>$inp['name'],'cat_url'=>$inp['url']
				));
			}elseif($inp['table'] == 'subcategory'){
				DB::table('sub_categories')->insert(array(
					'parent_id'=>$inp['parent'],'subcat_name'=>$inp['name'],'subcat_url'=>$inp['url']
				));
			}elseif($inp['table'] == 'goods'){
				DB::table('goods')->insert(array(
					'category_id'=>$inp['sub_cat'],'name'=>$inp['name'],'good_url'=>$inp['url'],'price'=>$inp['price']
				));
			}

			return Redirect::to('/admin/show/'.$inp['table']);
		}

		return Redirect::to('/admin');
	}
	
}

