<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use View,Request,Response,DB;
use App\Models\ServiceVoice as SV;

class AdminController extends Controller {


	public function index(){
		return View::make('admin.default');
	}

	public function getYml(){
		return View::make('admin.yml.yml');
	}

	public function serviceVoice(){
		
		$res = Request::all();
		// dd($res);
		//valid
		$sv = new SV();
		$sv->fio = $res['name'];
		$sv->email = $res['email'];
		$sv->text = $res['data'];
		$sv->created_at = new \DateTime;
		$sv->updated_at = new \DateTime;
		$sv->save();
		
		return Response::json(array( 'status' => 200));
	}

	public function getServiceVoice(){
		$ser = DB::table('servicevoices')->get();
		return View::make('admin.servicevoice.list')->with(['ser'=>$ser]);
	}

}
