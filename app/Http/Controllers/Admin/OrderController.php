<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use View,Request,DB,Redirect,Input,Auth;

class OrderController extends Controller {

	public function index(){
		$res = DB::table('orders')->get();
		return View::make('admin.orders.list')->with(['order'=>$res]);
	}

	public function cart($id){
		$res = DB::table('carts')
			->where('order_id','=',$id)
			->get();
		return View::make('admin.orders.cart')->with(['cart'=>$res]);
	}


}