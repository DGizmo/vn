<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use View,Request,Response;
use App\Models\ServiceVoice as SV;

class YmlController extends Controller {


	public function index(){
	
		$yml = '';

        if (isset($_POST['makeYml']))
		{
		  $result = '<?xml version="1.0" encoding="windows-1251"?>'."\n";
		  $result .= '<!DOCTYPE yml_catalog SYSTEM "shops.dtd">'."\n";
          $result .= '<yml_catalog date="'.date("Y-m-d H:i",time()).'">'."\n";
          $result .= '<shop>'."\n";
          $result .= '<name>'.mb_convert_encoding("Викки-Никки", "cp1251", "utf8").'</name>'."\n";
          $result .= '<company>'.mb_convert_encoding("ООО Викки-Никки", "cp1251", "utf8").'</company>'."\n";
          $result .= '<url>http://www.vikki-nikki.com/</url>'."\n";
          $result .= '<currencies>'."\n";
          $result .= '<currency id="RUR" rate="1" />'."\n";
          $result .= '</currencies>'."\n";
          $result .= '<categories>'."\n";
          $result .= '<category id="1">'.mb_convert_encoding("Девочки", "cp1251", "utf8").'</category>'."\n";
          $result .= '<category id="2">'.mb_convert_encoding("Мальчики", "cp1251", "utf8").'</category>'."\n";
          $result .= '<category id="3">'.mb_convert_encoding("Аксессуары", "cp1251", "utf8").'</category>'."\n";
          
          $result .= '<category id="4" parentId="1">'.mb_convert_encoding("Футболки, лонгсливы и топы", "cp1251", "utf8").'</category>'."\n";
          $result .= '<category id="5" parentId="1">'.mb_convert_encoding("Платья и сарафаны", "cp1251", "utf8").'</category>'."\n";
          $result .= '<category id="6" parentId="1">'.mb_convert_encoding("Юбки", "cp1251", "utf8").'</category>'."\n";
          $result .= '<category id="7" parentId="1">'.mb_convert_encoding("Штанишки и костюмы", "cp1251", "utf8").'</category>'."\n";
          $result .= '<category id="8" parentId="1">'.mb_convert_encoding("Толстовки", "cp1251", "utf8").'</category>'."\n";
          $result .= '<category id="9" parentId="2">'.mb_convert_encoding("Футболки", "cp1251", "utf8").'</category>'."\n";
          $result .= '<category id="10" parentId="2">'.mb_convert_encoding("Штанишки", "cp1251", "utf8").'</category>'."\n";
          $result .= '<category id="11" parentId="2">'.mb_convert_encoding("Толстовки", "cp1251", "utf8").'</category>'."\n";
          $result .= '<category id="12" parentId="3">'.mb_convert_encoding("Девочки", "cp1251", "utf8").'</category>'."\n";
          $result .= '<category id="13" parentId="3">'.mb_convert_encoding("Мальчики", "cp1251", "utf8").'</category>'."\n";
          

          $content_items = Content::model()->findAll(array('condition'=>'parent_id=3 AND status=1','order'=>'ordering'));
          $result .= '</categories>'."\n";
          $result .= '<local_delivery_cost>250</local_delivery_cost>'."\n";
          $result .= '<offers>'."\n";
          
          foreach ($content_items as $ci) {
              
              $products = Goods::model()->findAll(array('condition'=>'category_id='.$ci->id.' AND status=1','order'=>'ordering'));

              foreach($products as $pr){

                  $result .= '<offer id="'.$pr->id.'" available="true">'."\n";
                  $result .= '<url>http://www.vikki-nikki.com'.$ci->url.'/'.$pr->url.'</url>'."\n";
                  $result .= '<price>'.(($pr->new_price!=null)?$pr->new_price:$pr->price).'</price>'."\n";
                  $result .= '<currencyId>RUR</currencyId>'."\n";
                  $result .= '<categoryId>'.$ci->id.'</categoryId>'."\n";
                  if(isset($pr->photos[0]) && $pr->photos[0]->path!='') $result .= '<picture>http://www.vikki-nikki.com'.$pr->photos[0]->path.'</picture>'."\n";
                  $result .= '<delivery>true</delivery>'."\n";
                  $result .= '<name>'.mb_convert_encoding(htmlspecialchars($pr->name), "cp1251", "utf8").'</name>'."\n";
                  $result .= '<vendor>'.mb_convert_encoding("ООО Викки-Никки", "cp1251", "utf8").'</vendor>'."\n";
                  $result .= '<description>'.mb_convert_encoding(htmlspecialchars($pr->description), "cp1251", "utf8").'</description>'."\n";
                  $result .= '<country_of_origin>'.mb_convert_encoding("Россия", "cp1251", "utf8").'</country_of_origin>'."\n";
                  $result .= '<param name="'.mb_convert_encoding("Размеры", "cp1251", "utf8").'">';

                  //$models = $pr->sizes;
                  foreach ($pr->sizes as $model)
                  {
                    $sizes .= $model->sizes[$model->size]['minHeight'].'-'.$model->sizes[$model->size]['maxHeight'].',';
                  }
                  $sizes = substr($sizes,0,-1);
                  $result .=$sizes; unset($sizes);

                  $result .='</param>'."\n";
                  $result .= '</offer>'."\n";
              }
                                                  
          }

          $result .= '</offers>'."\n";
          $result .= '</shop>'."\n";
          $result .= '</yml_catalog>'."\n";

          file_put_contents($_SERVER['DOCUMENT_ROOT'].'/uploads/Vikki-Nikki_yml.xml', $result);

          $name = "yml_".date("Y-m-d").".yml";
          $file = fopen($_SERVER['DOCUMENT_ROOT'].'/uploads/YML_archiv/'.$name, "w");
          copy($_SERVER['DOCUMENT_ROOT'].'/uploads/Vikki-Nikki_yml.xml', $_SERVER['DOCUMENT_ROOT'].'/uploads/YML_archiv/'.$name);

		  
		  $yml = '<div><span style="color:green">Новый файл сгенерирован</span></div><br>';          	
		}

		$this->render('index', array(
			'yml' => $yml 
		));
	}



	}

}
