<?php namespace App\Http\Controllers;

use App\Models\Post;
use Input,DB,View;

class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	// public function __construct()
	// {
	// 	$this->middleware('auth');
	// }

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		//$posts = Post::get();
		//die(print_r($posts));
		return view('index');//->with('posts',$posts);
	}

	public function search()
	{
		// dd(1);
		$inp = Input::all();
		// dd($inp);
		$cat = DB::table('goods')
			->where('name','LIKE','%'.$inp['searchbar'].'%')
			->join('sub_categories', 'category_id', '=','subcat_id') 
			->join('main_categories', 'parent_id', '=','cat_id')
			->get();
		// dd($cat);
		return View::make('catalog.search')->with(['cat'=>$cat,'search'=>$inp['searchbar']]);
		//$posts = Post::get();
		//die(print_r($posts));
		// return view('index');//->with('posts',$posts);
	}
}
