<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Redirect,View,Cart,Auth,Input;
use App\Models\Order;
use App\Models\User as Myuser;
use App\Models\Cart as Mycart;


class OrderController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		if( Cart::count() ){ 
			return View::make('cart.order')->with('user',Auth::user());
		}else{
			return Redirect::to('/');
		}
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$res = Input::all();

		$user = Myuser::find(Auth::user()->id);
		foreach ($res as $key => $value) {
			if($user[$key] && $res[$key]){
				$user[$key] = $res[$key];
			}
		}
		$user->save();

		// dd($user);

		$order = new Order();
		$order->user_id = Auth::user()->id;
		$order->comment = $res['comment'];
		$order->sale = $res['sale'];

		$order->created_at = new \DateTime;
		$order->updated_at = new \DateTime;
		
		$tmp = 0;
		foreach (Cart::content() as $row) {
			$tmp += $row->price; 
		}
		$order->price = $tmp;

		$order->save();
		//dd($order);
		// dd(Cart::content());
		

		foreach (Cart::content() as $row) {
			$cart = new Mycart();
			$cart->user_id = Auth::user()->id;
			$cart->order_id = $order->id;
			$cart->good_name = $row->name;
			$cart->qty = $row->qty;
			$cart->price = $row->price;
			$cart->size = $row->options['size'];
			$cart->color = $row->options['color'];
			$cart->img = $row->options['img'];
			$cart->created_at = new \DateTime;
			$cart->updated_at = new \DateTime;
			$cart->save();
		}

		Cart::destroy();
		return Redirect::to('/');

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
