<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\User as Myuser;
use View,Auth,Redirect,Input,User;

class LKController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//dd(Auth::user()->name);
		// if(Auth::user()) {
		// 	$a = Auth::user();
		// 	Auth::logout();// = null;
		// 	return View::make('lk.index')->with('auth',$a);
		// }else {
		// 	Redirect::to('/');
		// }
		return View::make('lk.index')->with('auth',Auth::user());
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit()
	{
		//
		$res = Input::all();
		$user = Myuser::find(Auth::user()->id);
		// dd($res);
		foreach ($res as $key => $value) {
			if($key !="_token" && $res[$key]){
				$user[$key] = $res[$key];
			}
			
		}

		$user->save();

		// dd(Auth::user());
		return Redirect::back();
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update()
	{
		
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
