<?php namespace App\Http\Controllers;

use App\Models\Post;
use View,Input,DB;

class PostController extends Controller {

	public function index()
	{
		$posts = Post::get();
		return view('news.index')->with(['posts'=>$posts]);
	}

	public function getPost($slug){
		$post = Post::where('slug','=',$slug)->firstOrFail();
		$com = DB::table('comment_news')
			->where('news_id','=',$post->id)
			->get();

		return View::make('posts.show')->with(['post'=>$post,'comments'=>$com]);
	}

	public function postComment($slug){
		
		$inp = Input::all();

		$post = Post::where('slug','=',$slug)->firstOrFail();
		DB::table('comment_news')->insert(array(
			'news_id'=>$post->id,'user_id'=>$inp['user_id'],'text'=>$inp['text'],
			'user_name'=>$inp['user_name'],'created_at'=>new \DateTime,'updated_at'=>new \DateTime
			));
		return View::make('posts.show')->with('post',$post);
	}
}
