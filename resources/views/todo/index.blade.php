@extends('todo.layouts.default')

@section('content')
	<h1>Items</h1>
	<a href="{{ action('TodoController@getNew') }}">new task</a>
	<ul>
	   @foreach ($items as $item)
	        <li>
	        {!! Form::open() !!}
	            <input type="checkbox" name="item" id="item_{{ $item->id }}" {{ $item->done ? 'checked' : '' }}
	        onClick="this.form.submit()">       
	            <input type="hidden" name="item_id" value="{{ $item->id }}" />
	            <label for="item_{{ $item->id }}">{{ e($item->name)  }}</label>
	               <span><a href="{{ action('TodoController@getDelete', $item->id) }}">del</a></span>
	        {!! Form::close() !!}
	                     
	        </li>   
	        @endforeach
	</ul>
@stop