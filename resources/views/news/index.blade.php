@extends('layouts.default')

@section('content')

<h1>Новости</h1>

@if($posts->count())
	@foreach($posts as $post)
		<article>
			<h2>{{ $post->title }}</h2>
			<p>{{ Str::limit($post->body,3) }}</p>
			<a href="{{ action('PostController@getPost',$post->slug) }}">Читать далее...</a>
		</article>
	@endforeach
@endif




@stop
