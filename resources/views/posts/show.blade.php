@extends('layouts.default')

@section('content')

	<article>
		<h1>{{$post->title}}</h1>
		<p>Pub on {{ $post->created_at->format('d.m.Y H:i:s') }}</p>
		<p>{{$post->body}}</p>
	</article>
	<hr>
	@if($comments)
		<h3>Комментарии:</h3>
		@foreach($comments as $c)
			<article>
				<strong>{{$c->user_name}}</strong>  {{$c->updated_at}}
				<p>{{$c->text}}</p>
			</article>
		@endforeach
	@endif

	@if( Auth::user() )
		<h4>Вы можете оставить комментарий:</h4>
		{!! Form::open() !!}
			<input type="hidden" name="user_id" value="{{Auth::user()->id}}">
			<input type="hidden" name="user_name" value="{{Auth::user()->name}}">
			<textarea name="text" id="" cols="30" rows="10"></textarea><br>
			<input type="submit" value="Отправить">
		{!! Form::close() !!}
	@endif
@stop
