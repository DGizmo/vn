<!DOCTYPE html>
<html>
<head>
	<title>Vikki-Nikki Admin Board</title>
	{{-- <link rel="shortcut icon" href="/img/favicon-r.ico"> --}}
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	
	{{-- <link rel="stylesheet" href="/css/dataTables.css" type="text/css" media="all" /> --}}

	{{-- <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css" type="text/css" media="all" /> --}}
 	{{-- {{ HTML::style('bower_components/bootstrap/dist/css/bootstrap.min.css') }} --}}
     
	<link rel="stylesheet" href="{{URL::asset('bower_components/bootstrap/dist/css/bootstrap.min.css') }}" type="text/css" media="all" />
 	

	<link rel="stylesheet" href="{{URL::asset('css/spacelab-animate.css') }}" type="text/css" media="all" />
	<link rel="stylesheet" href="{{URL::asset('css/spacelab-font-awesome.min.css') }}" type="text/css" media="all" />
	<link rel="stylesheet" href="{{URL::asset('css/spacelab-main.css') }}" type="text/css" media="all" />
	
	<link rel="stylesheet" href="{{URL::asset('/plugins/icheck/css/grey.css') }}" type="text/css" media="all" />
	
	{{-- <link rel="stylesheet" href="/css/datepicker.css" type="text/css" media="all" /> --}}
	<link rel="stylesheet" href="{{URL::asset('css/space.css') }}" type="text/css" media="all" />
	
	<link rel="stylesheet" href="{{URL::asset('/css/common.css') }}" type="text/css" media="all" />
	<link rel="stylesheet" href="{{URL::asset('/plugins/messenger/css/messenger.css') }}" type="text/css" media="all" />
	<link rel="stylesheet" href="{{URL::asset('/plugins/messenger/css/messenger-theme-flat.css') }}" type="text/css" media="all" />
	<link rel="stylesheet" href="{{URL::asset('/plugins/messenger/css/location-sel.css') }}" type="text/css" media="all" />
	

	<link rel="stylesheet" href="{{URL::asset('/css/mycss.css') }}" type="text/css" media="all" />


    <script src="{{URL::asset('bower_components/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{URL::asset('bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="{{URL::asset('js/spacelab-application.js') }}"></script>
    
    {{-- <link rel="stylesheet" href="/js/paginator3000/paginator3000.css" type="text/css"></link>   --}}
	{{-- <script src="/js/paginator3000/paginator3000.js"></script>     --}}

    <script src="{{URL::asset('js/jquery.nanoscroller.min.js') }}"></script>
    <!--<script src="/js/dataTables.bootstrap.js"></script>-->

	<script src="{{URL::asset('/plugins/messenger/js/messenger.min.js') }}"></script>
	<script src="{{URL::asset('/plugins/messenger/js/messenger-theme-future.js') }}"></script>
	<script src="{{URL::asset('/plugins/messenger/js/custom_message.js') }}"></script>
    
    {{-- <script src="js/ui-bootstrap-tpls-0.10.0.min.js"></script> --}}
    <script src="{{URL::asset('/plugins/icheck/js/icheck.min.js') }}"></script>
    {{-- <script src="/js/chosen.jquery.min.js"></script>    --}}

</head>
<body>

	<section id="container">
		<header id="header">
			<!--logo start-->
			<div class="brand">
				<a href="/admin" class="logo" style="padding: 23px 0 0 50px!important;"> Vikki-Nikki  </a>
			</div>
			<!--logo end-->
			<div class="toggle-navigation toggle-left">
				<button type="button" class="btn btn-default" id="toggle-left" data-toggle="tooltip" data-placement="right" title="Показать/Скрыть панель">
					<i class="fa fa-bars"></i>
				</button>
			</div>
			<div class="user-nav">
				<ul>
				    <li class="profile-photo">
	                      {{-- <img src="#" class="img-circle" width="40px"> --}}
                    </li>
					<li class="dropdown settings">
						<span class="dropdown-toggle" data-toggle="dropdown" href="#">
							Admin <i class="fa fa-angle-down"></i>
						</span>
						<ul class="dropdown-menu animated fadeInDown">
							<li>
								<a href="/user/logout"><i class="fa fa-power-off"></i> Выйти</a>
							</li>
						</ul>
					</li>
				</ul>
			</div>
		</header>
		<!--sidebar start-->
		<aside class="sidebar sidebar-toggle">
			<div id="leftside-navigation" class="nano">
				<ul class="nano-content">
					<li><a href="/admin/show/category"><i class="fa fa-th-list"></i><span>Контент</span></a></li>
					<li><a href="/admin/orders"><i class="fa fa-list-alt"></i><span>Заказы</span></i></a></li>
					<li><a href="/admin/news"><i class="fa fa-list-alt"></i><span>Новости</span></i></a></li>
					<li><a href="/admin/comments"><i class="fa fa-list-alt"></i><span>Комментарии</span></a></li>
					<li><a href="/admin/servicevoice"><i class="fa fa-list-alt"></i><span>ServiceVoice</span></a></li>
					
					{{-- <li><a href=""><i class="fa fa-list-alt"></i><span>Файлы</span></a></li>
					<li><a href=""><i class="fa fa-list-alt"></i><span>Наборы</span></a></li>
					<li><a href=""><i class="fa fa-list-alt"></i><span>Магазин</span></a></li>
					<li><a href=""><i class="fa fa-list-alt"></i><span>Рассылки</span></a></li>
					<li><a href=""><i class="fa fa-list-alt"></i><span>Оптовики</span></a></li>
					<li><a href=""><i class="fa fa-list-alt"></i><span>Импорт</span></a></li>
					<li><a href=""><i class="fa fa-list-alt"></i><span>Цвета</span></a></li>
					<li><a href=""><i class="fa fa-list-alt"></i><span>Скидки</span></a></li>
					<li><a href=""><i class="fa fa-list-alt"></i><span>Чемпионы</span></a></li>
					<li><a href=""><i class="fa fa-list-alt"></i><span>Пароль</span></a></li>--}}
					<li><a href="/admin/yml"><i class="fa fa-list-alt"></i><span>YML</span></a></li>
					
				</ul>
			</div>

		</aside>
		<!--sidebar end-->
		<!--main content start-->
		<section class="main-content-wrapper">
			<section id="main-content">
				<div class="row">
					<div class="col-md-12">
					<div >
						
						@yield('content')

					</div>
						<hr>
						<footer class="footer">
								{{-- <div class="pull-left">
									TEST TEST DCP24 — <a href="http://dcp24.ru/">http://dcp24.ru/</a>
								</div> --}}
								<div class="pull-right" style="color:silver">
									DEV VERSION
								</div>
						</footer>
					</div>
				</div>
			</section>
		</section>
		<!--main content end-->

	</section>

</body>
</html>
<script>
$(document).ready(function(){
	$('aside').removeClass('sidebar-toggle');
})
</script>