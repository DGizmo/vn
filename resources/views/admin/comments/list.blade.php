@extends('admin.default')
@section('content')

	<div class="row">
		<div class="container">
			<div class="span4 offset4">
				<div class="span9">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3>Редактор комментариев
								<a href="/admin/comments/add" class="btn btn-success btn-mini btn-a" >
									<i class="fa fa-plus fa-white"></i>
								</a>
							</h3>
						</div>
						<div class="panel-body">
							{{-- <div>
								<a class="btn btn-success btn-a" href="">Комментарии к новостям</a>
								<a class="btn btn-success btn-a" href="">Комментарии к товарам</a>
							</div> --}}
							<table class="table">
								<thead>
									<th>Автор</th>
									<th>Комментарий</th>
									<th>Дата Создания</th>
									<th></th>
								</thead>
								<tbody>
									@foreach($comments as $c)
										<tr>
										<td>{{ $c->user_name }}</td>
										<td>{{ $c->text }}</td>
										<td>{{ $c->created_at }}</td>
										<td> <a href="/admin/comments/delete/{{$c->id}}" class="red">X</a></td>
										</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

@stop


