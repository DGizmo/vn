@extends('admin.default')
@section('content')

	<div class="row">
		<div class="container">
			<div class="span4 offset4">
				<div class="span9">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3>Оставить комментарий к новости</h3>
						</div>
						<div class="panel-body">
							{!! Form::open() !!}
								Название :
								<select name="new_id" >
									@foreach($posts as $p)
										<option value="{{$p->id}}">{{$p->title}}</option>
									@endforeach
								</select><br>
								Комментарий:
								<textarea name="text" id="" cols="30" rows="10" ></textarea><br><br>
								<a href="/admin/comments" class="btn btn-default">Отмена</a>
								<input type="submit" name="submit" value="Добавить" class="btn btn-success">
							{!! Form::close() !!}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

@stop


