@extends('admin.default')
@section('content')

	<div class="row">
		<div class="container">
			<div class="span4 offset4">
				<div class="span9">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3>Редактор новостей
								<a href="/admin/news/add" class="btn btn-success btn-mini btn-a" >
									<i class="fa fa-plus fa-white"></i>
								</a>
							</h3>
						</div>
						<div class="panel-body">
							<table class="table">
								<thead>
									<th>Название</th>
									<th>Содержание</th>
									<th>URL</th>
									<th>Дата Создания</th>
									<th></th>
									<th></th>
								</thead>
								<tbody>
									@foreach($posts as $post)
										<tr>
										<td><a href="/news/{{$post->slug}}"> {{ $post->title }} </a></td>
										<td>{{ Str::limit($post->body,20) }}</td>
										<td>{{ $post->slug }}</td>
										<td>{{ $post->created_at->format('d.m.Y H:i:s') }}</td>
										<td> <a href="/admin/news/edit/{{$post->id}}">Редактировать</a></td>
										<td> <a href="/admin/news/delete/{{$post->id}}" class="red">X</a></td>
										</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

@stop


