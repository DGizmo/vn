@extends('admin.default')
@section('content')

	<div class="row">
		<div class="container">
			<div class="span4 offset4">
				<div class="span9">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3>Создание новости</h3>
						</div>
						<div class="panel-body">
							{!! Form::open() !!}
								Название :<input type="text" name="name" ><br>
								URL: <input type="text" name="url" ><br><br>
								Текст новости:
								<textarea name="text" id="" cols="30" rows="10" ></textarea>
								<input type="hidden" name="id" ><br><br>
								<a href="/admin/news" class="btn btn-default">Отмена</a>
								<input type="submit" name="submit" value="Добавить" class="btn btn-success">
							{!! Form::close() !!}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

@stop


