@extends('admin.default')
@section('content')

	<div class="row">
		<div class="container">
			<div class="span4 offset4">
				<div class="span9">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3>Редактор новостей : {{$post->title}} </h3>
						</div>
						<div class="panel-body">
							{!! Form::open() !!}
								Название :<input type="text" name="name" value="{{$post->title}}"><br>
								URL: <input type="text" name="url" value="{{$post->slug}}"><br><br>
								Текст новости:
								<textarea name="text" id="" cols="30" rows="10" >{{$post->body}}</textarea>
								<input type="hidden" name="id" value="{{$post->id}}"><br><br>
								<a href="/admin/news" class="btn btn-default">Отмена</a>
								<input type="submit" name="submit" value="Добавить" class="btn btn-success">
							{!! Form::close() !!}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

@stop


