@extends('admin.default')
@section('content')

<div class="row">
	<div class="container">
		<div class="span4 offset4">
			<div class="span9">
				<div class="panel panel-default">
					{!! Form::open() !!}
						@if( $req['params'] == 'cat' )
							<div class="panel-heading"><h3>Добавление : Категории</h3></div>
							<div class="panel-body">
								Название :<input type="text" name="name"><br>
								URL: <input type="text" name="url"><br><br>
								<input type="hidden" name="table" value="category">
								<input type="submit" name="submit" value="Добавить" class="btn btn-success">
							</div>
						@elseif( $req['params'] == 'subcat' )
							<div class="panel-heading"><h3>Добавление : Подкатегории</h3></div>
							<div class="panel-body">
								Название :<input type="text" name="name"><br>
								Родительская категория :
								<select name="parent" >
									@foreach($parent as $p)
										<option value="{{$p->cat_id}}">{{$p->cat_name}}</option>
									@endforeach
								</select><br>
								URL: <input type="text" name="url"><br><br>
								<input type="hidden" name="table" value="subcategory">
								<input type="submit" name="submit" value="Добавить" class="btn btn-success">
							</div>
						@elseif( $req['params'] == 'good' )
							<div class="panel-heading"><h3>Добавление : Товара</h3></div>
							<div class="panel-body">
								Название :<input type="text" name="name"><br>
								Категория :
								<select name="cat" class="cat_select">
									<option class="empty" value=""></option>
									@foreach($cat as $c)
										<option value="{{$c->cat_id}}">{{$c->cat_name}}</option>
									@endforeach
								</select><br>
								<div class="wrap_sub hide">
									Подкатегория :
									<select name="sub_cat" class="sub_cat" >
										@foreach($sub_cat as $s)
											<option data-attr="{{$s->parent_id}}" value="{{$s->subcat_id}}">{{$s->subcat_name}}</option>
										@endforeach
									</select>
								</div>


								URL: <input type="text" name="url"><br>
								Цена: <input type="number" name="price"><br><br>
								<input type="hidden" name="table" value="goods">
								<input type="submit" name="submit" value="Добавить" class="btn btn-success">
							</div>
						@endif
							
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>

<script>
$(document).ready(function(){
	var arr = $('.sub_cat option');
	var len = arr.length;

	$('.cat_select').on('change',function(){
		$('.empty') ? $('.empty').remove() : '';
		$('.wrap_sub .hide') ? $('.wrap_sub').removeClass('hide') : '';
		var id = $(this).val();
		for (var i = 0; i < len; i++) {
			if( $(arr[i]).data('attr') != id ){
				$(arr[i]).addClass('hide');
			}else{
				$(arr[i]).removeClass('hide').prop('selected',true);
			}
		};

	});
});
</script>
@stop


