@extends('admin.default')
@section('content')

<div class="row">
	<div class="container">
		<div class="span4 offset4">
			<div class="span9">
				<div class="panel panel-default">
					<div class="panel-heading"><h3>Контент : Категории</h3></div>
					<div class="panel-body">
						<form class="form-horizontal" name="form">
							<table>
								<thead>
									<th>Название</th>
									<th></th>
									<th></th>
								</thead>
							</table>
							<tbody>
								@foreach($cat as $c)
									<tr>
										<td> <a href="/admin/goods/list?cat={{$c->cat_id}}">$c->cat_name</a> </td>
										<td> <a href="/admin/edit/category/{{$c->cat_id}}">Редактировать</a> </td>
										<td> <a href="/admin/delete/category/{{$c->cat_id}}">X</a> </td>

									</tr>
								@endforeach
							</tbody>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@stop


