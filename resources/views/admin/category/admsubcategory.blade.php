@extends('admin.default')
@section('content')

<div class="row">
	<div class="container">
		<div class="span4 offset4">
			<div class="span9">
				<div class="panel panel-default">
					<div class="panel-heading"><h3>Контент : Подкатегории</h3></div>
					<div class="panel-body">
						<form class="form-horizontal" name="form">
							<table>
								<thead>
									<th>Название</th>
									<th>Родительская категория</th>
									<th></th>
									<th></th>
								</thead>
							</table>
							<tbody>
								@foreach($sub as $s)
									<tr>
										<td> <a href="">$s->subcat_name</a> </td>
										<td> <a href="">$s->cat_name</a> </td>
										<td> <a href="">Редактировать</a> </td>
										<td> <a href="/admin/delete/subcategory/{{$s->subcat_id}}">X</a> </td>

									</tr>
								@endforeach
							</tbody>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@stop


