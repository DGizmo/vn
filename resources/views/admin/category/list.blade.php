@extends('admin.default')
@section('content')

<div class="row">
	<div class="container">
		<div class="span4 offset4">
			<div class="span9">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3>Контент : {{$header['name']}}
							<a href="/admin/add?params={{$header['url']}}" class="btn btn-success btn-mini btn-a" >
								<i class="fa fa-plus fa-white"></i>
							</a>
						</h3>
					</div>
					<div class="panel-body" >
						<div class="col-md-6">
							<div class="panel panel-default">
								<div class="panel-body">
								<div>
									<a class="btn btn-success btn-a" href="/admin/show/category">Категории</a>
									<a class="btn btn-success btn-a" href="/admin/show/subcategory">Подкатегории</a>
									<a class="btn btn-success btn-a" href="/admin/show/goods">Товары</a>
								</div>
									@if( $header['name'] == 'Категории')
										<table class="table">
											<thead>
												<tr>
													<td>Название</td>
													<td></td>
													<td></td>
												</tr>
											</thead>
											<tbody>
											@foreach($cat as $c)
												<tr>
													<td> <a href="/admin/show?cat={{$c->cat_id}}">{{$c->cat_name}}</a></td>
													<td> <a href="">Редактировать</a> </td>
													<td> <a href="/admin/delete?cat={{$c->cat_id}}" class="red">X</a> </td>
												</tr>
											@endforeach
											</tbody>
										</table>
									@elseif( $header['name'] == 'Подкатегории' )
										<table class="table">
											<thead>
												<tr>
													<td>Название</td>
													<td>Родительская категория</td>
													<td></td>
													<td></td>
												</tr>
											</thead>
											<tbody>
											@foreach($cat as $c)
												<tr>
													<td><a href="/admin/show?subcat={{$c->subcat_id}}">{{$c->subcat_name}}</a></td>
													<td> <a href="/admin/show?cat={{$c->cat_id}}">{{$c->cat_name}}</a></td>
													<td> <a href="">Редактировать</a> </td>
													<td> <a href="/admin/delete?subcat={{$c->subcat_id}}" class="red">X</a> </td>
												</tr>
											@endforeach
											</tbody>
										</table>
									@elseif( $header['name'] == 'Товары' )
										<table class="table">
											<thead>
												<tr>
													<td>Название</td>
													<td>Категория</td>
													<td>Подкатегория</td>
													<td></td>
													<td></td>
												</tr>
											</thead>
											<tbody>
											@foreach($cat as $c)
												<tr>
													<td><a href="/admin/show?good={{$c->id}}">{{$c->name}}</a></td>
													<td> <a href="/admin/show?cat={{$c->cat_id}}">{{$c->cat_name}}</a></td>
													<td><a href="/admin/show?subcat={{$c->subcat_id}}">{{$c->subcat_name}}</a></td>
													<td> <a href="">Редактировать</a> </td>
													<td> <a href="/admin/delete?good={{$c->id}}" class="red">X</a> </td>
												</tr>
											@endforeach
											</tbody>
										</table>
									@endif 

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@stop


