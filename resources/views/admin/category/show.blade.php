@extends('admin.default')
@section('content')

<div class="row">
	<div class="container">
		<div class="span4 offset4">
			<div class="span9">
				<div class="panel panel-default">
					<div class="panel-heading"><h3>Товары : {{$header}} </h3></div>
					<div class="panel-body" >
						<div class="col-md-6">
							<div class="panel panel-default">
								<div class="panel-body">
									<div>
										<a class="btn btn-success btn-a" href="/admin/show/category">Категории</a>
										<a class="btn btn-success btn-a" href="/admin/show/subcategory">Подкатегории</a>
										<a class="btn btn-success btn-a" href="/admin/show/goods">Товары</a>
									</div>
									<table class="table">
										<thead>
											<tr>
												<th>Название</th>
												<th>Подкатегория</th>
												<th>Характеристики</th>
												<th></th>
											</tr>
										</thead>
										<tbody>
										@foreach($cat as $c)
											<tr>
												<td> {{$c->name}} </td>
												<td><a href="/admin/show?subcat={{$c->subcat_id}}">{{$c->subcat_name}}</a></td>
												
												<td> <a href="/admin/edit/category/{{$c->cat_id}}">Редактировать</a> </td>
												<td> <a href="/admin/delete?good={{$c->id}}">X</a> </td>
											</tr>
										@endforeach
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@stop


