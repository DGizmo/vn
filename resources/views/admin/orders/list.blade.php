@extends('admin.default')
@section('content')

	<div class="row">
		<div class="container">
			<div class="span4 offset4">
				<div class="span9">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3>Список заказов</h3>
						</div>
						<div class="panel-body">
							<table class="table">
								<thead>
									<th>Дата Создания</th>
									<th>Покупатель</th>
									<th>Сумма к оплате</th>
									<th>Статус заказа</th>
									<th></th>
								</thead>
								<tbody>
									@foreach($order as $o)
										<tr>
										<td> {{ $o->created_at }} </td>
										<td> Денис </td>
										<td>{{ $o->price }}</td>
										<td>{{ $o->status }}</td>
										<td> <a href="/admin/orders/show/{{$o->id}}">Посмотреть</a></td>
										</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

@stop


