@extends('admin.default')
@section('content')

	<div class="row">
		<div class="container">
			<div class="span4 offset4">
				<div class="span9">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3>Список товаров в заказе</h3>
						</div>
						<div class="panel-body">
							<table class="table">
								<thead>
									<th>Товар</th>
									<th>Количество</th>
									<th>Стоимость</th>
									<th>Размер</th>
									<th>Цвет</th>
								</thead>
								<tbody>
									@foreach($cart as $c)
										<tr>
										<td> {{ $c->good_name }} </td>
										<td> {{ $c->qty }} </td>
										<td>{{ $c->price * $c->qty}}</td>
										<td>{{ $c->size }}</td>
										<td>{{ $c->color }}</td>
										</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

@stop


