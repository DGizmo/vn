@extends('admin.default')
@section('content')

	<div class="row">
		<div class="container">
			<div class="span4 offset4">
				<div class="span9">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3>ServiceVoice</h3>
						</div>
						<div class="panel-body">
							<table class="table">
								<thead>
									<th>ФИО</th>
									<th>E-mail</th>
									<th>Текст</th>
									<th>Дата создания</th>
								</thead>
								<tbody>
									@foreach($ser as $s)
										<tr>
										<td>{{ $s->fio }}</td>
										<td>{{ $s->email }}</td>
										<td>{{ $s->text }}</td>
										<td>{{ $s->created_at }}</td>
										</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

@stop


