<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="_token" content="{!! csrf_token() !!}" id="xtoken"/>
	<title>Internet Shop</title>
	<link rel="stylesheet" href="{{URL::asset('css/mycss.css') }}" type="text/css" media="all"></link>

	<link rel="stylesheet" href="{{URL::asset('bower_components/bootstrap/dist/css/bootstrap.min.css') }}" type="text/css" media="all"></link>
	
	<link rel="stylesheet" href="{{URL::asset('bower_components/bootstrap/dist/css/bootstrap-theme.min.css') }}" type="text/css" media="all"></link>

	{{-- <link rel="stylesheet" href="{{URL::asset('plugins/lumen.css') }}" type="text/css" media="all"></link> --}}

	<script src="{{URL::asset('bower_components/jquery/dist/jquery.min.js') }}"></script>
	<script src="{{URL::asset('bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
	
</head>
<body>

<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/">Vikki-Nikki</a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class=""><a href="/">Главная</a></li>
            <li><a href="/news">Новости</a></li>
          </ul>
          <div class="mypanel" >
		<a href="/cabinet">Личный кабинет {{Auth::user()? Auth::user()->name : ''}}</a>
		<a href="/cart">Корзина</a>:{{Cart::total()}}
	</div>
        </div><!--/.nav-collapse -->
      </div>
    </div>


	<a href="/">Main Page</a>
	<a href="/news">News</a>
	
	<div style="float:right">
		<a href="/cabinet">Личный кабинет {{Auth::user()? Auth::user()->name : ''}}</a>
		<a href="/cart">Корзина</a>:{{Cart::total()}}
	</div>
	<div class="searchbar">
		{!! Form::open(array('url'=>'/search')) !!}
		<input type="text" name="searchbar" value="{{isset($search) ? $search : ''}}"><input type="submit" value="Искать">
		{!! Form::close() !!}
	</div>
	


<ul id="menu">
    <li><a href="/catalog/girls">Девочки</a>
		<ul>
            <li><a href="/catalog/girls/gshirts">Футболки, лонгсливы и топы</a></li>
            <li><a href="/catalog/girls/gdress">Платья и сарафаны</a></li>
            <li><a href="/catalog/girls/gskirts">Юбки</a></li>
            <li><a href="/catalog/girls/gpants">Штанишки</a></li>
            <li><a href="/catalog/girls/gsmocks">Толстовки</a></li>
        </ul>
    </li>
    <li><a href="/catalog/boys">Мальчики</a>
        <ul>
            <li><a href="/catalog/boys/bshirts">Футболки</a></li>
            <li><a href="/catalog/boys/bpants">Штаны</a></li>
            <li><a href="/catalog/boys/bsmocks">Толстовки</a></li>
        </ul>
    </li>
    <li><a href="/catalog/accessories">Аксессуары</a>
        <ul>
            <li><a href="/catalog/accessories/agirls">Девочки</a></li>
            <li><a href="/catalog/accessories/aboys">Мальчики</a></li>
        </ul>
    </li>       
</ul>
	



	<div class="container mar40">
		@yield('content')
	</div>


	<link rel="stylesheet" href="{{URL::asset('plugins/serviceVoice/serviceVoice.css') }}" type="text/css" media="all"></link>
	<script src="{{URL::asset('bower_components/jquery/dist/jquery.min.js') }}"></script>
	<script src="{{URL::asset('plugins/serviceVoice/serviceVoice.js') }}"></script>
	<script>
	ServiceVoice.init();
	</script>
</body>
</html>

