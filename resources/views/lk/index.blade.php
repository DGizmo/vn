
@extends('layouts.default')

@section('content')
	<h1>Личный кабинет</h1>
	Здравствуйте, {{$auth->name}} <a href="/auth/logout">Выход</a>
	<h3>Персональные данные</h3>
	<strong>Ваш логин: </strong>{{$auth->name}}
	<br><strong>Ваш e-mail: </strong>{{$auth->email}}
	<br><br>
	
	{!! Form::open() !!}
		Фамилия
		<input type="text" name="surname" value="{{$auth->surname}}">
		<br>Имя
		<input type="text" name="firstname" value="{{$auth->firstname}}">
		<br>Отчество
		<input type="text" name="lastname" value="{{$auth->lastname}}">
		<br>Телефон
		<input type="tel" name="phone" value="{{$auth->phone}}">
		<br>Адресс
		<input type="text" name="adress" value="{{$auth->adress}}">
		<br><label for="getNews">
			<input type="checkbox" id="getNews" name="getNews" checked="{{$auth->getNews}}">
			Получать новости о скидках
		</label>
		<br><label for="getRefresh">
			<input type="checkbox" id="getRefresh" name="getRefresh" checked="{{$auth->getRefresh}}">
			Получать новости о скидках
		</label>
		<br><input type="submit" value="Сохранить">
	{!! Form::close() !!}
	
	

@stop