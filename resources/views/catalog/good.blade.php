@extends('layouts.default')

@section('content')

	<h1>{{ $good->name }}</h1>

	<h3>Price: {{ $good->price }}</h3>


    {!! Form::open() !!}
    {{-- <input type="checkbox" name="item" id="item_{{ $item->id }}" {{ $item->done ? 'checked' : '' }}
onClick="this.form.submit()">       
    <input type="hidden" name="item_id" value="{{ $item->id }}" />
    <label for="item_{{ $item->id }}">{{ e($item->name)  }}</label>
       <span><a href="{{ action('TodoController@getDelete', $item->id) }}">del</a></span>
	 --}}
	Количество:
	<select name="qty" >
		<option value="1">1</option>
		<option value="2">2</option>
		<option value="3">3</option>
	</select>
	Цвет:
	<select name="color" >
		<option value="red">red</option>
		<option value="green">green</option>
		<option value="blue">blue</option>
	</select>
	Размер:
	<select name="size" >
		<option value="128-134">128-134</option>
		<option value="134-138">134-138</option>
		<option value="138-144">138-144</option>
	</select>

	<input type="hidden" name="item_id" value="{{ $good->id }}" />
	<input type="hidden" name="name" value="{{ $good->name }}" />
	<input type="hidden" name="price" value="{{ $good->price }}" />
	<input type="hidden" name="img" value="test_img" />
	
	
	
	@if( Auth::user() )
		<button type="submit">Добавить в корзину</button>
	@else 
		<a href="/auth/login">Требуется авторизация</a>
	@endif 
	
	{!! Form::close() !!}


	about good

@stop