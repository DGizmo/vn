@extends('layouts.default')

@section('content')

	<h3>Результаты поиска:</h3>

	@foreach($cat as $c)

		<h3>
			<a href="/catalog/{{ $c->cat_url}}/{{$c->subcat_url}}/{{ $c->good_url}}"> {{ $c->name}} </a>
		</h3>
	
	@endforeach
	
	
@stop