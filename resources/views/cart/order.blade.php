
@extends('layouts.default')

@section('content')
	<h1>Оформление заказа</h1>
	{!! Form::open() !!}
	<table  class="table">
		<tbody>
			<tr>
				<th>Имя</th>
				<td><input type="text" name="name" value="{{$user->firstname}}"></td>
			</tr>
			<tr>
				<th>Фамилия</th>
				<td><input type="text" name="surname" value="{{$user->surname}}"></td>
			</tr>
			<tr>
				<th>Телефон</th>
				<td><input type="text" name="phone" value="{{$user->phone}}"></td>
			</tr>
			{{-- <tr>
				<th>Город</th>
				<td><input type="text" name="city" value="{{$user->city}}"></td>
			</tr> --}}
			<tr>
				<th>Адрес</th>
				<td><input type="text" name="adress" value="{{$user->adress}}"></td>
			</tr>
			<tr>
				<th>E-mail</th>
				<td><input type="text" name="email" value="{{$user->email}}"></td>
			</tr>
			<tr>
				<th>Примечание к заказу</th>
				<td><textarea name="comment" id="" cols="30" rows="10"></textarea></td>
			</tr>
			<tr>
				<th>Код скидки</th>
				<td><input type="text" name="sale" ></td>
			</tr>
				{{-- <tr>
					<td>{{$c->options['img']}}</td>
					<td>{{$c->name}}</td>
					<td>{{$c->options['color']}}</td>
					<td>{{$c->options['size']}}</td>
					<td>{{$c->price}}</td>
					<td>{{$c->qty}}</td>
					<td>{{$c->qty*$c->price}}</td>
					<td> <a href="{{ action('CartController@destroy', $c->rowid) }}"> X </a></td>
				</tr> --}}
			
		</tbody>
	</table>
	<label for="check">
		<input type="checkbox" name="check" id="check">
		Я подтверждаю свое согласие с условиями <a href="/offer">договора оферты</a>
	</label>
	<input type="submit" value="Оформить">
	{!! Form::close() !!}
@stop