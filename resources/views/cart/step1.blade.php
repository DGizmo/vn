
@extends('layouts.default')

@section('content')
	
	<table class="table">
		<thead>
			<tr>
				<th>Изображение</th>
				<th>Название</th>
				<th>Цвет</th>
				<th>Размер</th>
				<th>Цена</th>
				<th>Количество</th>
				<th>Стоимость</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			@foreach($cart as $c)
				<tr>
					<td>{{$c->options['img']}}</td>
					<td>{{$c->name}}</td>
					<td>{{$c->options['color']}}</td>
					<td>{{$c->options['size']}}</td>
					<td>{{$c->price}}</td>
					<td>{{$c->qty}}</td>
					<td>{{$c->qty*$c->price}}</td>
					<td> <a href="{{ action('CartController@destroy', $c->rowid) }}"> X </a></td>
				</tr>
			@endforeach
		</tbody>
	</table>
	<a href="/cart/order">Далее</a>
@stop